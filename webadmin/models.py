from datetime import datetime
from flask_login import UserMixin
from werkzeug.security import check_password_hash
from werkzeug.security import generate_password_hash

from webadmin.extensions import db
from webadmin.extensions import login


@login.user_loader
def load_user(id):
    return User.query.get(int(id))

#TODO create model schema and add comments to describes entities

class User(UserMixin, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    password_hash = db.Column(db.String(128))

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<User {}>'.format(self.username)


class Host(db.Model):
    __tablename__ = 'hosts'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, unique=True)
    ssh_target = db.Column(db.String(64), index=True, unique=False)
    ssh_user = db.Column(db.String(64), index=True, unique=False)
    ssh_password = db.Column(db.String(128), index=True, unique=False)
    ssh_connexion_mode = db.Column(db.String(16), index=True, unique=False)

    operations = db.relationship("Operation", back_populates="host")

    def __repr__(self):
        return '<SSH_host target:{} user:{} mode:{}>'.format(self.ssh_target, self.ssh_user, self.ssh_connexion_mode)


class OperationRun(db.Model):
    __tablename__ = 'operation_runs'
    id = db.Column(db.Integer, primary_key=True)
    run_datetime = db.Column(db.DateTime)

    operation_id = db.Column(db.Integer, db.ForeignKey('operations.id'), unique=True)
    operation = db.relationship("Operation", back_populates="runs")

# class ScheduledRun(db.Model):
# to schedule verification operations (rerun playbooks), backups and non critical upgrades


class Operation(db.Model):
    __tablename__ = 'operations'

    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String(16))

    __mapper_args__ = {
        'polymorphic_identity':'operation',
        'polymorphic_on': type
    }

    name = db.Column(db.String(64), index=True, unique=True)
    modification = db.Column(db.Boolean, index=True) # modification (new changes) or verification (check state)
    creation_datetime = db.Column(db.DateTime)
    nb_steps = db.Column(db.Integer, index=True)

    host_id = db.Column(db.Integer, db.ForeignKey('hosts.id'), unique=True)
    host = db.relationship("Host", back_populates="operations")

    runs = db.relationship("OperationRun", back_populates="operation")

    #template_id = db.Column(db.Integer, db.ForeignKey('operation_templates.id'))


class AnsibleOperation(Operation):
    __tablename__ = 'ansible_operations'
    __mapper_args__ = {
        'polymorphic_identity':'ansible_operation',
    }
    id = db.Column(db.Integer, db.ForeignKey('operations.id'), primary_key=True)

    plays = db.relationship("AnsiblePlay", back_populates="ansible_operation")


plays_roles_association = db.Table('plays_roles_association', db.metadata,
    db.Column('play_id', db.Integer, db.ForeignKey('ansible_plays.id')),
    db.Column('role_id', db.Integer, db.ForeignKey('ansible_roles.id'))
)


class AnsibleRole(db.Model):
    __tablename__ = 'ansible_roles'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True) # modification (first run = changes) or verification (check state)
    variables = db.Column(db.JSON)

    plays_using_role = db.relationship(
        "AnsiblePlay",
        secondary=plays_roles_association,
        back_populates="ansible_roles")


class AnsiblePlay(db.Model):
    __tablename__ = 'ansible_plays'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(16), index=True)
    variables_values = db.Column(db.JSON)

    ansible_operation_id = db.Column(db.Integer, db.ForeignKey('ansible_operations.id'))
    ansible_operation = db.relationship("AnsibleOperation", back_populates="plays")
    ansible_roles = db.relationship(
        "AnsibleRole",
        secondary=plays_roles_association,
        back_populates="plays_using_role"
    )




# class Resource(db.Model):
#     __tablename__ = 'resources'
#     id = db.Column(db.Integer, primary_key=True)
#     type = db.Column(db.String(16), index=True)
#     fields = db.Column(db.JSON)
#     #backups = db.Column(db.Integer, db.ForeignKey('backups.id'))
#
#
# class ServerTemplate(db.Model):
#     __tablename__ = 'server_templates'
#     id = db.Column(db.Integer, primary_key=True)
#     name = db.Column(db.String(64), index=True, unique=True)
#     description = db.Column(db.JSON)
#     #host_id
#     #host
#
#
#
# class OperationTemplate(db.Model):
#     __tablename__ = 'operation_templates'
#     id = db.Column(db.Integer, primary_key=True)
#     server_template_id = db.Column(db.Integer, db.ForeignKey('server_templates.id'))
#     #server_template
#
#
# class Backup(db.Model):
#     __tablename__ = 'backups'
#     id = db.Column(db.Integer, primary_key=True)
#     #resource_id
#     #resource


def populate():
    host = Host(
        name='testounet',
        ssh_user='root',
        ssh_target='10.42.42.42',
        ssh_connexion_mode='ssh_key'
    )

    modification = AnsibleOperation(
        type="ansible_operation",
        name="install grav",
        modification=True,
        creation_datetime=datetime.now(),
        nb_steps=-1,
        host_id=1,
        host=host,
    )

    roles = [
        AnsibleRole(
            name="test-role",
            variables={
                "var1": 2,
                "var2": "default"
            }
        )
    ]

    plays = [
        AnsiblePlay(
            name="play-1",
            variables_values={
                "var1": 1,
                "var2": "truc"
            },
            ansible_operation=modification,
            ansible_roles=roles,
        ),
        AnsiblePlay(
            name="play-2",
            variables_values={
                "var1": 1,
                "var2": "truc"
            },
            ansible_operation=modification,
            ansible_roles=roles,
        ),
    ]

    modification.plays = plays

    db.session.add(modification)
    db.session.commit()

    return modification
