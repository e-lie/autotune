import dash_html_components as html
import re


#TODO use css classes instead of element css

def create_output_area(id):
    return html.Pre(
        id=id,
        className="bg-light p-5",
        style={
            "height": 600,
            'overflowY': 'scroll',
            'overflowX': 'hidden',
            'whiteSpace': 'initial',
        }
    )


def create_green_span(text):
    return html.Span(text, style={'color': "green"})

def create_colored_span_rgb(text, rgb_color):
    return html.Span(text, style={'color': "rgb{}".format(rgb_color)})

def create_yellow_span(text):
    return html.Span(text, style={'color': "yellow"})

def create_colored_span(text, bootstrap_color):
    return html.Span(text, className="{}".format(bootstrap_color))


class ANSICodeTranslator:

    def __init__(self):
        self.COLOR_DICT = {
            0: [(0, 0, 0), (0, 0, 0)],
            31: [(180, 0, 0), (128, 0, 0)],
            32: [(0, 180, 0), (0, 128, 0)],
            33: [(180, 180, 0), (128, 128, 0)],
            34: [(0, 0, 180), (0, 0, 128)],
            35: [(180, 0, 180), (128, 0, 128)],
            36: [(0, 180, 180), (0, 128, 128)],
        }
        self.BOOTRAPCOLOR_DICT = {
            0: ["text-dark"],
            31: ["text-danger"],
            32: ["text-success"],
            33: ["text-warning"],
            34: ["text-primary"],
            35: ["text-secondary"],
            36: ["text-info"],

        }

    def ansi_to_span_list(self, text):
        """
        print ansi_to_html('[06-10-13 21:28:23] [INFO] [0;31;1mUsage: /kick [reason ...][m')
        print ansi_to_html('[06-10-13 21:28:23] [INFO] [31;0mUsage: /kick [reason ...][m')
        print ansi_to_html('[06-10-13 21:28:23] [INFO] [31mUsage: /kick [reason ...][m')

        [06-10-13 21:28:23] [INFO] <span style="color: rgb(128, 0, 0); font-weight: bolder">Usage: /kick [reason ...]</span>
        [06-10-13 21:28:23] [INFO] <span style="color: rgb(180, 0, 0)">Usage: /kick [reason ...]</span>
        [06-10-13 21:28:23] [INFO] <span style="color: rgb(180, 0, 0)">Usage: /kick [reason ...]</span>
        """
        resulting_html = []

        # *? means non greedy = the .*? match stop first time it encounters \[ for example
        # DOTALL means that . matches \n
        matches = re.finditer(
            r'(?P<pre_text>.*?)\[(?P<arg_1>\d+)(;(?P<arg_2>\d+)(;(?P<arg_3>\d+))?)?m(?P<span_text>.*?)\[0m(?P<post_text>.*?)',
            string=text, flags=re.DOTALL)

        for match in matches:
            pre, span, post = match.group('pre_text'), match.group('span_text'), match.group('post_text')
            arg1, arg2, arg3 = match.group('arg_1'), match.group('arg_2'), match.group('arg_3')
            if arg3 is None:
                if arg2 is None:
                    bold, color = arg1, 0
                else:
                    bold, color = arg1, int(arg2)
            else:
                bold, color = arg2, int(arg3)

            if pre:
                resulting_html.append(pre)
            if bold != '0':
                resulting_html.append(create_colored_span(span, self.BOOTRAPCOLOR_DICT[color][0]))
            else:
                resulting_html.append(create_colored_span(span, self.BOOTRAPCOLOR_DICT[color][0]))
            if post:
                resulting_html.append(post)

        if not resulting_html:
            resulting_html.append(text)

        return resulting_html