from webadmin.autotune_ui.modifications.modifications_endpoints import modification_index
from webadmin.autotune_ui.operations.operations_endpoints import operations_index
from .hosts.hosts_endpoints import hosts_index, host_add_page, host_show_page
from webadmin.models import *

from .hosts.hosts_callbacks import hosts_callbacks
from .modifications.modification_callbacks import modification_callbacks
from .operations.operations_callbacks import operations_callbacks

from .base_layout import full_layout, display_menu_column
from .operations.operations_components import operations_content
from .hosts.hosts_components import *
from .modifications.modification_components import *


def serve_layout():
    return full_layout



def admin_index():
    roles = Host.query.all()
    return hosts_page(roles), display_menu_column('hosts-link')


def page_not_found(pathname):
    return html.Div([html.H3("Page {} doesn't exist :/".format(pathname))]), display_menu_column('')

def register_callbacks(dashapp):

    # Common callbacks

    @dashapp.callback(
        [
            Output('content-column', 'children'),
            Output('menu-column', 'children'),
        ],
        [Input('url', 'pathname')],
    )
    def display_page(pathname="/admin"):

        pathname = pathname.rstrip('/')

        if pathname == '/admin': result = admin_index()

        elif pathname == '/admin/hosts': result = hosts_index()
        elif pathname == '/admin/hosts/host-add': result = host_add_page()
        elif pathname.startswith('/admin/hosts/host-'): result = host_show_page(pathname)

        elif pathname == '/admin/modification': result = modification_index()

        elif pathname == '/admin/operations': result = operations_index()

        elif pathname == '/admin/populate':
            result = populate()
            return html.H2("populated ! {}".format(result)), display_menu_column('modification-link')

        else: result = page_not_found(pathname)

        return result


    # Specific callbacks
    hosts_callbacks(dashapp)
    modification_callbacks(dashapp)
    operations_callbacks(dashapp)

