from dash.dependencies import Output, Input, State


from webadmin.extensions import db
from webadmin.models import Host

def hosts_callbacks(dashapp):

    @dashapp.callback([
                          Output('add-host-name-input', 'valid'),
                          Output('add-host-user-input', 'valid'),
                          Output('add-host-target-input', 'valid'),
                          Output('add-host-password-input', 'valid'),
                          Output('check-ssh-button', 'disabled'),
                          Output('add-host-button', 'disabled'),
                      ],
                      [
                          Input('add-host-name-input', 'value'),
                          Input('add-host-user-input', 'value'),
                          Input('add-host-target-input', 'value'),
                          Input('add-host-password-input', 'value'),
                          Input('add-host-id-method-input', 'value'),
                      ])
    def add_host_validation(name_value, user_value, target_value, identification_value, id_method_value):

        name_valid, user_valid, target_valid, identification_valid = (False, False, False, False)

        if name_value:
            name_valid = True
        if user_value:
            user_valid = True
        if target_value:
            target_valid = True
        if identification_value:
            identification_valid = True

        button_disabled = not (
                name_valid and
                user_valid and
                target_valid and
                (
                    identification_valid or id_method_value != 'ssh-password'
                )
        )
        return name_valid, user_valid, target_valid, identification_valid, button_disabled, button_disabled


    @dashapp.callback([Output('host-ssh-pass-fade', 'is_in')],[Input('add-host-id-method-input', 'value')])
    def fade_password_input(id_method_value):
        if id_method_value == 'ssh-password':
            return  [True]
        if id_method_value == 'ssh-key':
            return [False]
        else:
            return [False]


    @dashapp.callback(Output('add-host-button', 'children'),
                      [Input('add-host-button', 'n_clicks')],
                      [
                          State('add-host-name-input', 'value'),
                          State('add-host-user-input', 'value'),
                          State('add-host-target-input', 'value'),
                          State('add-host-id-method-input', 'value'),
                          State('add-host-password-input', 'value'),
                      ])
    def add_host(n_clicks,
                 name_value,
                 user_value,
                 target_value,
                 id_method_value,
                 password_value):
        if n_clicks:
            new_host = Host(
                name=name_value,
                ssh_user=user_value,
                ssh_target=target_value,
                #ssh_connexion_type=id_method_value,
                #ssh_password = password_value,
            )
            db.session.add(new_host)
            db.session.commit()
            return 'Added' #display_host_added_message()
        return 'Add Host'
