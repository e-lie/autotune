import dash_bootstrap_components as dbc
import dash_core_components as dcc
from webadmin.autotune_ui.component_factories import *
from parse import parse
from dash.dependencies import Input
from dash.dependencies import Output
from dash.dependencies import State
from webadmin.models import Host, db



def host_add_form():
    return dbc.FormGroup([
        html.H3("Add New Host", className="my-4"),
        dbc.Row([
            dbc.Col([
                dbc.Label("Name of the Host", html_for="example-email-grid"),
                dbc.Input(
                    type="text",
                    id="add-host-name-input",
                    placeholder="Name",
                ),
                dbc.FormText("SSH User (unix) to connect with..."),
            ], xl=6),
        ]),
        dbc.Row([
            dbc.Col([
                dbc.Label("Username", html_for="example-email-grid"),
                dbc.Input(
                    type="text",
                    id="add-host-user-input",
                    placeholder="SSH user",
                ),
                dbc.FormText("SSH User (unix) to connect with..."),
            ], xl=4),
            dbc.Col([
                dbc.Label("Target", html_for="example-email-grid"),
                dbc.Input(
                    type="text",
                    id="add-host-target-input",
                    placeholder="Target",
                ),
                dbc.FormText("IP address or domain name of the server"),
            ], xl=8),
        ]),
        dbc.Row([
            dbc.Col([
                dbc.Label('Identification method'),
                dbc.RadioItems(
                    options=[
                        {"label": "SSH key (generate it and add it manually)", "value": "ssh-key"},
                        {"label": "Password", "value": "ssh-password"},
                    ],
                    value="ssh-key",
                    id="add-host-id-method-input",
                ),
            ], xl=5, className="mt-4"),
            dbc.Col([
                dbc.Fade([
                    dbc.Label("Password"),
                    dbc.Input(
                        type='password',
                        id="add-host-password-input",
                        placeholder="SSH password"
                    ),
                ], id="host-ssh-pass-fade", is_in=False)
            ], xl=7, className="mt-4"),
        ]),
        dbc.Row([
            dbc.Col([
                dbc.Button("Check Connection", id="check-ssh-button", block=True, color="secondary", disabled=True),
            ], xl=6, className="mt-5"),
            dbc.Col([
                dbc.Button("Add Host", id="add-host-button", block=True, color="success", disabled=True),
            ], xl=6, className="mt-5"),
        ]),
    ], id="host-add-form")


def hosts_list(hosts, active_host_number):

    host_list_items = [
        dbc.ListGroupItem(
            dbc.ListGroupItemHeading([html.H2("Hosts")])
        )
    ]


    host_list_items += [
        dbc.ListGroupItem(
            [
                dbc.ListGroupItemHeading(host.name),
                dbc.ListGroupItemText("Some test server"),
            ],
            id="host-{}".format(host.id),
            href="/admin/hosts/host-{}".format(host.id),
            action=True,
            active=(host.id == active_host_number),
        )
        for host in hosts
    ]

    host_list_items.append(
        dbc.ListGroupItem(
            dbc.Button("Add host +", block=True, size="md", color="success"),
            id="host-add",
            href="/admin/hosts/host-add",
        )
    )

    return host_list_items


def host_overview(host):
    return [
        dbc.Card(
            [
                dbc.CardHeader(html.H2(host.name)),
                dbc.CardBody(
                    [
                        #dbc.CardTitle(),
                        html.P(
                            [
                                html.H4('SSH Login: {host.ssh_user}@{host.ssh_target}'.format(host=host)),
                                dbc.Row([
                                    dbc.Col([
                                        dbc.Button("Update infos", color="secondary", block=True),
                                    ]),
                                    dbc.Col([
                                        dbc.Button("Delete X !", color="danger", block=True),
                                    ])
                                ]),
                            ], className='card-text',
                        ),
                    ]
                ),
            ],
            className="mt-4"
        ),
    ]


def hosts_initial_content(hosts):
    return [] if not len(hosts) else host_overview(hosts[0])


def hosts_page(hosts, host_content=None, host_list=None):

    if host_list is None:
        if not hosts:
            host_id = -1
        else:
            host_id = hosts[0].id
        host_list = hosts_list(hosts, host_id)

    if host_content is None:
        host_content = hosts_initial_content(hosts)

    hosts_content = [
        dbc.Row([
            dbc.Col([
                #html.H3('Hosts', className="my-4"),
                dbc.ListGroup(
                    children=host_list,
                    id='host-list',
                    className='my-4'
                )
            ], md=3),
            dbc.Col(
                children=host_content,
                id="hosts-content-column",
                md=9,
                className="px-md-5"),
        ], ),
    ]

    return hosts_content


def host_added_message():
    return [
        html.H3('Add New Host'),
        html.H3('Host Added !', color="success", className="mt-5")
    ]



