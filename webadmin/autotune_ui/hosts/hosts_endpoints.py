from parse import parse

from webadmin.autotune_ui.base_layout import display_menu_column
from .hosts_components import hosts_page, host_add_form, hosts_list, host_overview
from webadmin.models import Host


def hosts_index():
    roles = Host.query.all()
    return hosts_page(roles), display_menu_column('hosts-link')


def host_add_page():
    roles = Host.query.all()
    return (
        hosts_page(roles, host_add_form(), hosts_list(roles, -1)),
        display_menu_column('hosts-link')
    )

def host_show_page(pathname):
    roles = Host.query.all()
    host_num = int(parse('/admin/hosts/host-{}', pathname)[0])
    host = [host for host in roles if host.id == host_num][0]
    return (
        hosts_page(roles, host_overview(host), hosts_list(roles, host_num)),
        display_menu_column('hosts-link')
    )