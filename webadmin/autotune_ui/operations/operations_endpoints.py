from webadmin.autotune_ui.base_layout import display_menu_column
from webadmin.autotune_ui.operations.operations_components import operations_content


def operations_index():
    return operations_content, display_menu_column('operations-link')