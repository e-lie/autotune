from dash.dependencies import Input
from dash.dependencies import Output
from webadmin.extensions import ansible_wrapper
from webadmin.models import Host


def operations_callbacks(dashapp):
    @dashapp.callback(Output('live-update-text', 'children'), [Input('interval-component', 'n_intervals')])
    def update_sdtout(n_intervals):
        return ansible_wrapper.stdout_lines


    @dashapp.callback(Output('interval-component', 'disabled'),
                      [Input('interval-component', 'n_intervals'),
                   Input('submit-button', 'n_clicks')])
    def check_process_running(n_intervals, n_clicks):
        return not ansible_wrapper.running


    @dashapp.callback(Output('submit-button', 'children'), [Input('submit-button', 'n_clicks')])
    def launch_process(n_clicks):
        result = 'Launch Playbook'
        if n_clicks > 0:
            hosts = Host.query.all()
            if hosts:
                ansible_wrapper.update_inventory(hosts)
                ansible_wrapper.launch_process()
                result = "Playbook launched"
            else:
                result = "No host configured"
        return result

