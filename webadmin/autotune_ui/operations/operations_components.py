import dash_html_components as html
import dash_bootstrap_components as dbc
import dash_core_components as dcc
from webadmin.autotune_ui.component_factories import *


process_watcher = dcc.Interval(id='interval-component', interval=1*800, n_intervals=0 )




operations_content = [
                        html.H1('Ansible process'),
                        dbc.Row([
                            dbc.Col([
                                dbc.Button(id='submit-button', n_clicks=0, children='Launch playbook'),
                            ], md=4),
                            dbc.Col([
                                dbc.Progress(id="progress", value=0, striped=True, animated=True),
                            ], md=8),
                        ], className="mt-5"),
                        dbc.Row([
                            dbc.Col([
                                create_output_area('live-update-text'),
                            ], width=12)
                        ], className="mt-4"),
                        process_watcher
                    ]








