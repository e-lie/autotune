import dash_bootstrap_components as dbc
import dash_html_components as html


def display_modification_list(modifications, active_modification_number):

    modification_list_items = [
        dbc.ListGroupItem(
            dbc.ListGroupItemHeading([html.H3("Modifications")])
        )
    ]

    modification_list_items += [
        dbc.ListGroupItem(
            [
                dbc.ListGroupItemHeading(modification.name),
                dbc.ListGroupItemText("A nice role"),
            ],
            id="host-{}".format(modification.id),
            href="/admin/hosts/host-{}".format(modification.id),
            action=True,
            active=(modification.id == active_modification_number),
        )
        for modification in modifications
    ]

    modification_list_items.append(
        dbc.ListGroupItem(
            dbc.Button("New modification +", block=True, size="md", color="success"),
            id="host-add",
            href="/admin/hosts/modification-add",
        )
    )

    return modification_list_items


def display_modification_overview(modification):

    plays_tabs = dbc.Tabs(
        [
            dbc.Tab(label=play.name, className="")
            for play in modification.plays
        ],
        card=True
    )


    return [
        dbc.Card(
            [
                dbc.CardHeader(
                    [
                        html.H2(modification.name),
                        plays_tabs
                    ]

                ),
                dbc.CardBody(

                ),
            ],
            className="mt-4"
        ),
    ]

def display_modification_role(role):
    return

def display_initial_modification_content(modifications):
    if modifications:
        return display_modification_overview(modifications[0])
    else:
        return []


modification_add_form = dbc.FormGroup([
    html.H3("Add New Modification", className="my-4"),
    dbc.Row([
        dbc.Col([
            dbc.Label("Name of the Host", html_for="example-email-grid"),
            dbc.Input(
                type="text",
                id="add-host-name-input",
                placeholder="Name",
            ),
            dbc.FormText("SSH User (unix) to connect with..."),
        ], xl=6),
    ]),
    dbc.Row([
        dbc.Col([
            dbc.Label("Username", html_for="example-email-grid"),
            dbc.Input(
                type="text",
                id="add-host-user-input",
                placeholder="SSH user",
            ),
            dbc.FormText("SSH User (unix) to connect with..."),
        ], xl=4),
        dbc.Col([
            dbc.Label("Target", html_for="example-email-grid"),
            dbc.Input(
                type="text",
                id="add-host-target-input",
                placeholder="Target",
            ),
            dbc.FormText("IP address or domain name of the server"),
        ], xl=8),
    ]),
    dbc.Row([
        dbc.Col([
            dbc.Label('Identification method'),
            dbc.RadioItems(
                options=[
                    {"label": "SSH key (generate it and add it manually)", "value": "ssh-key"},
                    {"label": "Password", "value": "ssh-password"},
                ],
                value="ssh-key",
                id="add-host-id-method-input",
            ),
        ], xl=5, className="mt-4"),
        dbc.Col([
            dbc.Fade([
                dbc.Label("Password"),
                dbc.Input(
                    type='password',
                    id="add-host-password-input",
                    placeholder="SSH password"
                ),
            ], id="host-ssh-pass-fade", is_in=False)
        ], xl=7, className="mt-4"),
    ]
    ),

    dbc.Row([
        dbc.Col([
            dbc.Button("Check Connection", id="check-ssh-button", block=True, color="secondary", disabled=True),
        ], xl=6, className="mt-5"),
        dbc.Col([
            dbc.Button("Add Host", id="add-host-button", block=True, color="success", disabled=True),
        ], xl=6, className="mt-5"),
    ]),
], id="host-add-form")




def display_modification_page(modifications, modification_content=None, modification_list=None):

    if modification_list is None:
        if not modifications:
            modification_id = -1
        else:
            modification_id= modifications[0].id
        modification_list = display_modification_list(modifications, modification_id)

    if modification_content is None:
        modification_content = display_initial_modification_content(modifications)

    modification_page_content = [
        dbc.Row([
            dbc.Col([
                dbc.ListGroup(
                    children=modification_list,
                    id='host-list',
                    className='my-4'
                )
            ], md=3),
            dbc.Col(
                children=modification_content,
                id="hosts-content-column",
                md=9,
                className="px-md-5"),
        ], ),
    ]

    return modification_page_content

