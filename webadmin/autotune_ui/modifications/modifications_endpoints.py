from webadmin.autotune_ui.base_layout import display_menu_column
from webadmin.autotune_ui.modifications.modification_components import display_modification_page
from webadmin.models import Operation


def modification_index():
    modification_query = Operation.query.filter(Operation.modification == True)
    modifications = modification_query.all()
    return display_modification_page(modifications), display_menu_column('modification-link')