
import dash_html_components as html
import dash_bootstrap_components as dbc
import dash_core_components as dcc

navbar = dbc.NavbarSimple(
            children=[
                dbc.NavItem([
                    dcc.LogoutButton(label='Cassons-nous', logout_url='/logout', className="btn btn-primary")
                ]),
            ],
            brand="Autotune",
            brand_href="/admin",
            #sticky="top",
            fluid=True,
            color='dark',
            dark=True,
        )



def display_menu_column(link_id):
    return  [
                html.Div([
                    dbc.Nav(
                        [
                            dbc.NavItem(dbc.NavLink(
                                'Hosts', id="hosts-link",
                                href='/admin/hosts', active=(link_id == 'hosts-link'))),
                            dbc.NavItem(dbc.NavLink(
                                'Modify host', id="modification-link",
                                href='/admin/modification', active=(link_id == 'modification-link'))),
                            dbc.NavItem(dbc.NavLink(
                                'Pending operations', id="operations-link",
                                href='/admin/operations', active=(link_id == 'operations-link'))),
                        ],
                        vertical="lg",
                        className="pt-3",
                        pills=True,
                    )
                ],
                )
            ]



full_layout = html.Div(
        [
            #control the browser adress bar
            dcc.Location(id='url', refresh=False),

            # Top navigation bar
            navbar,

            dbc.Container([
                    dbc.Row(
                        [
                            dbc.Col(children=display_menu_column(''), id='menu-column', lg=3, xl=2),
                            dbc.Col([], id="content-column", lg=9, xl=10, className="px-5 mt-4"),
                        ]
                    )
                ],
                fluid=True,
            )
        ]
    )