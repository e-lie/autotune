import dash
import dash_bootstrap_components as dbc
from flask import Flask
from flask.helpers import get_root_path
from flask_login import login_required

from config import BaseConfig


def create_app():
    server = Flask(__name__)
    server.config.from_object(BaseConfig)

    register_dashapps(server)
    register_extensions(server)
    register_blueprints(server)

    return server


def register_dashapps(app):
    from webadmin.autotune_ui import serve_layout
    from webadmin.autotune_ui import register_callbacks

    # Meta tags for viewport responsiveness
    meta_viewport = {"name": "viewport", "content": "width=device-width, initial-scale=1, shrink-to-fit=no"}

    autotune_ui = dash.Dash(
        __name__,
        server=app,
        url_base_pathname='/admin/',
        assets_folder=get_root_path(__name__) + '/assets/',
        external_stylesheets=[dbc.themes.LUMEN],
        meta_tags=[meta_viewport]
    )

    autotune_ui.config.suppress_callback_exceptions = True

    autotune_ui.title = 'Autotune'
    autotune_ui.layout = serve_layout()
    register_callbacks(autotune_ui)
    _protect_dashviews(autotune_ui)


def _protect_dashviews(dashapp):
    for view_func in dashapp.server.view_functions:
        if view_func.startswith(dashapp.url_base_pathname):
            dashapp.server.view_functions[view_func] = login_required(dashapp.server.view_functions[view_func])


def register_extensions(server):
    from webadmin.extensions import db
    from webadmin.extensions import login
    from webadmin.extensions import migrate
    from webadmin.extensions import ansible_wrapper

    db.init_app(server)
    login.init_app(server)
    login.login_view = 'main.login'
    migrate.init_app(server, db)
    ansible_wrapper.init_app(server)


def register_blueprints(server):
    from webadmin.webapp import server_bp

    server.register_blueprint(server_bp)
