import os
import ansible_runner
import dash_html_components as html
from webadmin.autotune_ui.component_factories import ANSICodeTranslator

class AnsibleWrapper:
    def __init__(self, app=None, runner_env='runner_env', inventory='inventory/hosts'):
        self.stdout_lines = []
        self.running = False
        self.runner_env = runner_env
        self.inventory = inventory
        self.ansi_parser = ANSICodeTranslator()
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        if not hasattr(app, 'extensions'):
            app.extensions = {}
        app.extensions['ansible_wrapper'] = self
        self.app = app

    def update_inventory(self, hosts):
        """
        Write inventory file from operation data in db
        :param hosts:
        :return:
        """
        content = """
        [autotune]
        """
        for host in hosts:
            content += """
        autotune.box ansible_host={} ansible_user={}
        """.format(host.ssh_target, host.ssh_user)

        with open(os.path.join(self.runner_env, self.inventory), 'w+') as inventory:
           inventory.write(content)

    def event_callback(self, event_dict):
        stdout_line = event_dict["stdout"]
        html_elements = self.ansi_parser.ansi_to_span_list(stdout_line)
        self.stdout_lines.append(html.P(html_elements))

    def finished_callback(self, runner_obj):
        self.running = False

    def launch_process(self):
        self.running = True

        thread, runner_obj = ansible_runner.run_async(
            quiet=True,
            private_data_dir=self.runner_env,
            event_handler=self.event_callback,
            finished_callback=self.finished_callback,
            playbook='apply.yml')

        return runner_obj


