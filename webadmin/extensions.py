from flask_login import LoginManager
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from .ansible_wrapper import AnsibleWrapper

db = SQLAlchemy()
migrate = Migrate()
login = LoginManager()

ansible_wrapper = AnsibleWrapper(runner_env='runner_env')
