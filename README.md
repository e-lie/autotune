
# Autotune

Autotune (development name) is an attempt to provide a good UX over modern adminsys/devops tools to make servers manageable by non technical users. It's currently based upon Ansible, a dash web ui, and some default server configuration (heavily inspired by the YunoHost configuration).

It's still at an early development stage.

# Installation (dev install on Linux/MacOS)

1. Generate a ssh-pubkey
```
ssh-keygen -t ed25519
cat /your/public/keyfile.pub
```
2. Add key to provision vagrant dev VM:
   1. edit `vagrant/pubkey.yml.dist`
   2. rename it to `pubkey.yml`

3. Launch VM then test ssh connexion
```bash
cd vagrant
vagrant up
ssh root@10.42.42.42
```

4. Install the webapp dependencies
```bash
cd .. # (go to the root of git repo)
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt
```

5. Load app environment and init database
```
source .env
flask db init
flask db migrate -m init
flask db upgrade
```

6. Unlock ssh key (for ansible), and launch the app
```bash
eval $(ssh-agent) && ssh-add your/private/key
flask run
```

