#!/usr/bin/env python

"""
"""

import argparse
import ast
import os
import re
import requests
import sys
from time import time

try:
    import ConfigParser
except ImportError:
    import configparser as ConfigParser

try:
    import json
except ImportError:
    import simplejson as json




class AutotuneInventory(object):

    ###########################################################################
    # Main execution path
    ###########################################################################

    def __init__(self):
        self.build_inventory()

        print(json.dumps(self.inventory))



    def read_cli_args(self):
        """ Command line argument processing """
        parser = argparse.ArgumentParser(description='Produce an Ansible Inventory file based on Autotune database')

        parser.add_argument('--list', action='store_true', help='List all active Droplets as Ansible inventory (default: True)')
        parser.add_argument('--host', action='store', help='Get all Ansible inventory variables about a specific Droplet')

        self.args = parser.parse_args()

        # Make --list default if none of the other commands are specified
        if  not self.args.host:
            self.args.list = True



    def build_inventory(self):
        self.inventory = {
                            "_meta": {
                                "hostvars": {
                                    "autotune.box": {
                                        "ansible_host": "10.42.42.42",
                                        "ansible_user": "root",
                                        "inventory_dir": "/home/elie/Documents/2018-2019/hiyu/autotune/misc",
                                        "inventory_file": "/home/elie/Documents/2018-2019/hiyu/autotune/misc/hosts.ini"
                                    }
                                }
                            },
                            "all": {
                                "children": [
                                    "ungrouped",
                                    "autotune"
                                ],
                                "hosts": [],
                                "vars": {}
                            },
                            "autotune": {
                                "children": [],
                                "hosts": [
                                    "autotune.box"
                                ],
                                "vars": {}
                            },
                            "ungrouped": {
                                "children": [],
                                "hosts": [],
                                "vars": {}
                            }
                        }



###########################################################################
# Run the script
AutotuneInventory()
